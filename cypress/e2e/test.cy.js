describe('Test todo list creation basic features', () => {
	Cypress.Commands.add('createTodo', (title) => {
		cy.get('input[id="title"]').type(JSON.stringify(title));	
		cy.get('button[type="submit"]').first().click();
	})
	before(()=>{
		cy.visit('/');
		cy.get('button[type="button"]').click();
		cy.contains("Add Todo");	
	})

	it('Check Todo Creation', () => {
		cy.createTodo('MyFirstTodo');
		cy.contains('Total Todos: 1');	
  	})

	it('Check Todo Count' , () => {	
		cy.createTodo('MyFirstTodo');
		cy.createTodo('MySecondTodo');
		cy.createTodo('MyThirdTodo');
		cy.contains('Total Todos: 3');
	})

	it('Check Todo Selection', () => {
		cy.createTodo('MyFirstTodo');
		cy.get('input[type="checkbox"]').first().click();
		cy.contains('Selected Todos: 1');
	})


})
